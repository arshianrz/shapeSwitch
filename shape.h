#ifndef SHAPE_H
#define SHAPE_H

class Rectangle
{
private:
    int Length_;  //Length
    int Width_;  //Width
public:
    Rectangle();
    void setLength(int);
    int getLength();
    void setWidth(int);
    int getWidth();
    void rectanglePrint(int,int);
    void runRect();

};

class Triangle
{
private:
    int n_,temp_;
public:
    Triangle();
    void setN(int);
    int getN();
    void setTemp(int);
    void display();
    void runTri();

};

#endif // SHAPE_H
