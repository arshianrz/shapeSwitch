#include "shape.h"
#include <iostream>
using namespace std;

//************************************************************************************************************
                                         //Rectangle
Rectangle::Rectangle()
{
    runRect();
}
void Rectangle::setLength(int A)
{
    Length_=A;
}

int Rectangle::getLength()
{
    return Length_;
}

void Rectangle::setWidth(int B)
{
    Width_=B;
}

int Rectangle::getWidth()
{
    return Width_;
}

void Rectangle::rectanglePrint(int p, int q)
{
    for(int i=1 ; i<=p ; i++)
    {
        for(int j=1 ; j<=q ; j++)
        {
           if (i==1 || j==1 || i==p || j==q)
           {
               cout<<"*";
           }
           else
               cout<<" ";
        }
     cout<<endl;
    }
}

void Rectangle::runRect()
{
    int x,y;
    cout<<"Enter length.";
    cin>>x;
    cout<<"Enter width.";
    cin>>y;

    setLength(x);
    getLength();
    setWidth(y);
    getWidth();
    rectanglePrint(x,y);
}
//-------------------------------------------------------------------------------------------------------------
                                            //Triangle
Triangle::Triangle()
{
    setTemp(1);
    runTri();

}

void Triangle::setN(int N)
{
    n_=N;
}

int Triangle::getN()
{
    return n_;
}

void Triangle::setTemp(int TEMP)
{
    temp_=TEMP;
}

void Triangle::display()
{
    for(int i=n_ ; i>=1 ; i--)
    {
        if(i==1)
        {
            for(int l=1 ; l<=(2*n_) ; l++)
            {
                if (l%2==0)
                    cout<<" ";
                else
                    cout<<"*";
            }
            break;
        }


        for(int k=i-1 ; k>=1 ; k--)
        {
            cout<<" ";
        }

        cout<<"*";

        for(int k=2 ; k<=temp_-1 ; k++ )
        {
            cout<<" ";
        }
        temp_+=2;

        if(i==n_)
        {
            cout<<" ";
        }
        else
        {
            cout<<"*";
        }
        cout<<endl;
    }
    cout<<endl;
}

void Triangle::runTri()
{
    int a;
    cout<<"Enter Dimension of your triangle. \n";
    cin>>a;
    setN(a);
    getN();
    display();
}
