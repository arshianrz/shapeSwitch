#include <iostream>
#include <iomanip>
#include "shape.h"
using namespace std;
void selectShape();

int main()
{
    selectShape();
    return 0;
}

void selectShape()
{
    int x;
    cout<<"What kind of shape do want to print? \n";
    cout<<"To print rectangle"<<setw(10)<<"Press 1"<<endl;
    cout<<"To print triangle "<<setw(10)<<"Press 2"<<endl;
    cin>>x;

    switch (x)
    {
    case 1:
        Rectangle();
        break;

    case 2:
        Triangle();
        break;

    default:
        break;
    }

}
